/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.importadresses.controller;

import org.eclipse.openk.gridfailureinformation.importadresses.AddressImportApplication;
import org.eclipse.openk.gridfailureinformation.importadresses.jobs.JobManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = AddressImportApplication.class)
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class AddressImportControllerTest {

//    @MockBean
//    private JobManager jobManager;
//
//    @Autowired
//    private MockMvc mockMvc;
//
//    @Test
//    @WithMockUser(value = "mockedUser")
//    public void shouldTriggerStartImport() throws Exception {
//
//        mockMvc.perform(post("/addresses/import"))
//                .andExpect(status().is2xxSuccessful());
//
//        verify(jobManager).triggerStartImport();
//    }
//
//    @Test
//    public void shouldTriggerStartImportAndReturnUnauthorized() throws Exception {
//
//        mockMvc.perform(post("/addresses/import"))
//                .andExpect(status().isUnauthorized());
//
//        verify(jobManager, times(0)).triggerStartImport();
//    }
}
