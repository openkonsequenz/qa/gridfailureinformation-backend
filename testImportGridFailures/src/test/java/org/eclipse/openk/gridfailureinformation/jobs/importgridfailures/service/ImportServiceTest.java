/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.service;

import org.eclipse.openk.gridfailureinformation.jobs.importgridfailures.config.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

public class ImportServiceTest {
    @Qualifier("myBranchService")
    @Autowired
    private ImportService importService;

    @MockBean
    private ImportService messageImportChannel;

//    @Test
//    public void shouldPushFailure() {
//
//        ForeignFailureDto foreignFailureDto = MockDataHelper.mockForeignFailureDto();
//
//        ImportService spy = Mockito.spy(messageImportChannel);
//       Mockito.doNothing().when(spy).send(any(Message.class));
//
//        importService.pushForeignFailure(foreignFailureDto);
//    }
}
