/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Request;
import feign.Response;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.StoerungsauskunftInterfaceApplication;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.api.StoerungsauskunftApi;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.ForeignFailureMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.StoerungsauskunftUserNotification;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.exceptions.InternalServerErrorException;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.support.MockDataHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = StoerungsauskunftInterfaceApplication.class)
@ContextConfiguration(classes = {TestConfiguration.class})
@ActiveProfiles("test")
public class ImportExportServiceTest {

    @Autowired
    private ImportExportService importExportService;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    StoerungsauskunftApi stoerungsauskunftApi;

    @Test
    public void shoulImportUserNotification() throws IOException {

        ImportExportService importExportServicSpy = spy(importExportService);

        InputStream is = new ClassPathResource("UsernotificationJsonResponse.json").getInputStream();
        List<StoerungsauskunftUserNotification> userNotificationList =
                objectMapper.readValue(is, new TypeReference<List<StoerungsauskunftUserNotification>>() {
                });
        when(stoerungsauskunftApi.getUserNotification(any(Integer.class))).thenReturn(userNotificationList);

        importExportServicSpy.importUserNotifications();

        verify(importExportServicSpy, times(userNotificationList.size())).pushForeignFailure(any(ForeignFailureMessageDto.class));
    }

    @Test
    public void shouldExportForeignFailureMessageDto() throws ParseException {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        Request request = mock(Request.class);
        Response mockResponse = Response.builder().status(200).request(request).body("Test", StandardCharsets.UTF_8).build();

        when(stoerungsauskunftApi.postOutage(any(), anyBoolean())).thenReturn(mockResponse);
        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);
        verify(stoerungsauskunftApi, times(1)).postOutage(anyList(), anyBoolean());
    }

    @Test
    public void shouldExportForeignFailureMessageDto_planned_polygons() throws ParseException {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        rabbitMqMessageDto.getFailureInformationDto().setPlanned(true);
        rabbitMqMessageDto.getFailureInformationDto().setAddressPolygonPoints(MockDataHelper.mockPolygonCoordinatesList());

        Request request = mock(Request.class);
        Response mockResponse = Response.builder().status(200).request(request).body("Test", StandardCharsets.UTF_8).build();

        when(stoerungsauskunftApi.postOutage(any(), anyBoolean())).thenReturn(mockResponse);
        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);
        verify(stoerungsauskunftApi, times(1)).postOutage(anyList(), anyBoolean());
    }

    @Test
    public void shouldExportForeignFailureMessageDto_planned_polygons_empty() throws ParseException {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        rabbitMqMessageDto.getFailureInformationDto().setPlanned(true);
        rabbitMqMessageDto.getFailureInformationDto().setAddressPolygonPoints(new ArrayList<>());

        Request request = mock(Request.class);
        Response mockResponse = Response.builder().status(200).request(request).body("Test", StandardCharsets.UTF_8).build();

        when(stoerungsauskunftApi.postOutage(any(), anyBoolean())).thenReturn(mockResponse);
        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);
        verify(stoerungsauskunftApi, times(1)).postOutage(anyList(), anyBoolean());
    }

    @Test
    public void shouldThrowErrorWhenCallToExternalInterfacefailed() throws ParseException {
        RabbitMqMessageDto rabbitMqMessageDto = MockDataHelper.mockRabbitMqMessageDto();

        Request request = mock(Request.class);
        Response mockResponse = Response.builder().status(400).request(request).body("Test", StandardCharsets.UTF_8).build();

        when(stoerungsauskunftApi.postOutage(any(), anyBoolean())).thenReturn(mockResponse);
        assertThrows(InternalServerErrorException.class, () -> importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto));

    }

    @Test
    public void shoulImportUserNotificationMapperTest1() throws IOException {

        ImportExportService importExportServicSpy = spy(importExportService);

        InputStream is = new ClassPathResource("UsernotificationJsonResponse.json").getInputStream();
        List<StoerungsauskunftUserNotification> userNotificationList =
                objectMapper.readValue(is, new TypeReference<List<StoerungsauskunftUserNotification>>() {
                });
        when(stoerungsauskunftApi.getUserNotification(any(Integer.class))).thenReturn(userNotificationList);

        importExportServicSpy.importUserNotifications();

        verify(importExportServicSpy, times(userNotificationList.size())).pushForeignFailure(any(ForeignFailureMessageDto.class));
    }


}
