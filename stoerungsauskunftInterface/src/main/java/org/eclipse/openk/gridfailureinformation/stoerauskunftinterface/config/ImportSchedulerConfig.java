/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.config;

import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@Log4j2
@Configuration
@EnableScheduling
@ConditionalOnProperty(prefix = "stoerungsauskunft.scheduling-import", name = "enabled", havingValue = "true", matchIfMissing = false)
public class ImportSchedulerConfig {

    private static final String SCHEDULER_NAME = "Stoerauskunftimport-Scheduler";

    @Autowired
    ImportExportService importExportService;

    @Value("${stoerungsauskunft.scheduling-import.cron}")
    private String cronExpression;

    @Bean
    public void logConfigLdap() {
        log.info(SCHEDULER_NAME + " is enabled with cron expression: " + cronExpression);
    }

    @Scheduled(cron = "${stoerungsauskunft.scheduling-import.cron}")
    public void scheduleTaskImportUserNotifications() {
        log.info("Executing" + SCHEDULER_NAME + " task: Importing available usernotifications from stoerungsauskunft.de");
        importExportService.importUserNotifications();
        log.info("Finished " + SCHEDULER_NAME + " task: Importing available usernotifications from stoerungsauskunft.de");
    }
}
