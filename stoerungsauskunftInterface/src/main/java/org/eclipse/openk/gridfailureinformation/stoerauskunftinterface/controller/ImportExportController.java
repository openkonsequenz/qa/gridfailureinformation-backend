/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.dtos.RabbitMqMessageDto;
import org.eclipse.openk.gridfailureinformation.stoerauskunftinterface.service.ImportExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Log4j2
@RestController
@RequestMapping("/stoerungsauskunft")
public class ImportExportController {

    @Autowired
    private ImportExportService importExportService;

    @GetMapping("/usernotification-import-test")
    @ApiOperation(value = "Import einer externen Störungsinformation von Störungsauskunft.de zu SIT")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformation erfolgreich importiert"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void importUserNotification() {
        importExportService.importUserNotifications();
    }

    @PostMapping("/outage-export-test")
    @ApiOperation(value = "Export einer Störungsinformation von SIT zu Störungsauskunft.de")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Störungsinformation erfolgreich exportiert"),
            @ApiResponse(code = 500, message = "Konnte nicht durchgeführt werden")
    })
    @ResponseStatus(HttpStatus.OK)
    public void postOutage(@RequestBody RabbitMqMessageDto rabbitMqMessageDto)  {
        importExportService.exportStoerungsauskunftOutage(rabbitMqMessageDto);
    }

}
