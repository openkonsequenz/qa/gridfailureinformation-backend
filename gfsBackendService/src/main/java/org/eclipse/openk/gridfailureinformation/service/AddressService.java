/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.constants.Constants;
import org.eclipse.openk.gridfailureinformation.exceptions.NotFoundException;
import org.eclipse.openk.gridfailureinformation.mapper.AddressMapper;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.HousenumberUuidDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;

    private static HousenumberUuidDto toHousenumberUuid(TblAddress tblAddress) {
        HousenumberUuidDto housenumberUuidDto = new HousenumberUuidDto();
        housenumberUuidDto.setHousenumber(tblAddress.getHousenumber());
        housenumberUuidDto.setUuid(tblAddress.getUuid());
        return housenumberUuidDto;
    }

    public List<AddressDto> getAddresses(Optional<String> branchOpt) {
        List<TblAddress> addressList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                addressList = addressRepository.findByPowerConnection(true);
                break;
            case Constants.G:
                addressList = addressRepository.findByGasConnection(true);
                break;
            case Constants.W:
                addressList = addressRepository.findByWaterConnection(true);
                break;
            case Constants.F:
                addressList = addressRepository.findByDistrictheatingConnection(true);
                break;
            case Constants.TK:
                addressList = addressRepository.findByTelecommConnection(true);
                break;
            default:
                addressList = addressRepository.findAll();
        }

        return addressList.stream()
                .map(addressMapper::toAddressDto)
                .collect(Collectors.toList());
    }

    public AddressDto getAdressByUuid(UUID uuid) {
        TblAddress tblAddress = addressRepository.findByUuid(uuid)
                .orElseThrow(NotFoundException::new);
        return addressMapper.toAddressDto(tblAddress);
    }

    public List<String> getPostcodes(Optional<String> branchOpt) {
        List<String> postcodeList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                postcodeList = addressRepository.findAllPostcodesForPower();
                break;
            case Constants.G:
                postcodeList = addressRepository.findAllPostcodesForGas();
                break;
            case Constants.W:
                postcodeList = addressRepository.findAllPostcodesForWater();
                break;
            case Constants.F:
                postcodeList = addressRepository.findAllPostcodesForDistrictheating();
                break;
            case Constants.TK:
                postcodeList = addressRepository.findAllPostcodesForTelecomm();
                break;
            default:
                postcodeList = addressRepository.findAllPostcodes();
        }

        return postcodeList;
    }


    public List<String> getCommunities(Optional<String> branchOpt) {
        List<String> communityList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                communityList = addressRepository.findAllCommunitysForPower();
                break;
            case Constants.G:
                communityList = addressRepository.findAllCommunitysForGas();
                break;
            case Constants.W:
                communityList = addressRepository.findAllCommunitysForWater();
                break;
            case Constants.F:
                communityList = addressRepository.findAllCommunitysForDistrictheating();
                break;
            case Constants.TK:
                communityList = addressRepository.findAllCommunitysForTelecomm();
                break;
            default:
                communityList = addressRepository.findAllCommunitys();
        }

        return communityList.stream().filter(Objects::nonNull).filter(c -> !c.isEmpty()).collect(toCollection(ArrayList::new));
    }


    public List<String> getDistricts(String community, Optional<String> branchOpt) {
        List<String> districtList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                districtList = addressRepository.findDistrictsByCommunityForPowerConnections(community);
                break;
            case Constants.G:
                districtList = addressRepository.findDistrictsByCommunityForGasConnections(community);
                break;
            case Constants.W:
                districtList = addressRepository.findDistrictsByCommunityForWaterConnections(community);
                break;
            case Constants.F:
                districtList = addressRepository.findDistrictsByCommunityForDistrictheatingConnections(community);
                break;
            case Constants.TK:
                districtList = addressRepository.findDistrictsByCommunityForTelecommConnections(community);
                break;
            default:
                districtList = addressRepository.findDistrictsByCommunity(community);
        }

        return districtList;
    }

    public List<String> getCommunities(String postcode, Optional<String> branchOpt) {
        List<String> communityList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                communityList = addressRepository.findCommunitiesByPostcodeForPowerConnections(postcode);
                break;
            case Constants.G:
                communityList = addressRepository.findCommunitiesByPostcodeForGasConnections(postcode);
                break;
            case Constants.W:
                communityList = addressRepository.findCommunitiesByPostcodeForWaterConnections(postcode);
                break;
            case Constants.F:
                communityList = addressRepository.findCommunitiesByPostcodeForDistrictheatingConnections(postcode);
                 break;
            case Constants.TK:
                communityList = addressRepository.findCommunitiesByPostcodeForTelecommConnections(postcode);
                break;
            default:
                communityList = addressRepository.findCommunitiesByPostcode(postcode);
        }

        return communityList;
    }


    public List<String> getPostcodes(String community, String district, Optional<String> branchOpt) {
        List<String> postcodeList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                postcodeList = addressRepository.findPostcodesByCommunityAndDistrictForPower(community, district);
                break;
            case Constants.G:
                postcodeList = addressRepository.findPostcodesByCommunityAndDistrictForGas(community, district);
                break;
            case Constants.W:
                postcodeList = addressRepository.findPostcodesByCommunityAndDistrictForWater(community, district);
                break;
            case Constants.F:
                postcodeList = addressRepository.findPostcodesByCommunityAndDistrictForDistrictheating(community, district);
                break;
            case Constants.TK:
                postcodeList = addressRepository.findPostcodesByCommunityAndDistrictForTelecomm(community, district);
                break;
            default:
                postcodeList = addressRepository.findPostcodesByCommunityAndDistrict(community, district);
        }
        return postcodeList;
    }


    public List<String> getDistricts(String postcode, String community, Optional<String> branchOpt) {
        List<String> districtList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                districtList = addressRepository.findDistrictsByPostcodeAndCommunityForPower(postcode, community);
                break;
            case Constants.G:
                districtList = addressRepository.findDistrictsByPostcodeAndCommunityForGas(postcode, community);
                break;
            case Constants.W:
                districtList = addressRepository.findDistrictsByPostcodeAndCommunityForWater(postcode, community);
                break;
            case Constants.F:
                districtList = addressRepository.findDistrictsByPostcodeAndCommunityForDistrictheating(postcode, community);
                break;
            case Constants.TK:
                districtList = addressRepository.findDistrictsByPostcodeAndCommunityForTelecomm(postcode, community);
                break;
            default:
                districtList = addressRepository.findDistrictsByPostcodeAndCommunity(postcode, community);
        }
        return districtList;
    }

    public List<String> getStreets(String postcode, String community, Optional<String> district, Optional<String> branchOpt) {
        List<String> streetList;
        String branch = branchOpt.orElseGet(String::new);

        if (!district.isPresent()) {
            switch (branch) {
                case Constants.S:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityForPower(postcode, community);
                    break;
                case Constants.G:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityForGas(postcode, community);
                    break;
                case Constants.W:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityForWater(postcode, community);
                    break;
                case Constants.F:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityForDistrictheating(postcode, community);
                    break;
                case Constants.TK:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityForTelecomm(postcode, community);
                    break;
                default:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunity(postcode, community);
            }
            return streetList;
        } else {
            switch (branch) {
                case Constants.S:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForPower(postcode, community, district.get());
                    break;
                case Constants.G:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForGas(postcode, community, district.get());
                    break;
                case Constants.W:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForWater(postcode, community, district.get());
                    break;
                case "F":
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForDistrictHeating(postcode, community, district.get());
                    break;
                case "TK":
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityAndDistrictForTelecomm(postcode, community, district.get());
                    break;
                default:
                    streetList = addressRepository.findStreetsByPostcodeAndCommunityAndDistrict(postcode, community, district.get());
            }
            return streetList;
        }

    }

    public List<HousenumberUuidDto> getHousenumbers(String postcode, String community, String street, Optional<String> branchOpt) {
        List<TblAddress> addressListList;
        String branch = branchOpt.orElseGet(String::new);

        switch (branch) {
            case Constants.S:
                addressListList = addressRepository.findByPostcodeAndCommunityAndStreetForPower(postcode, community, street);
                break;
            case Constants.G:
                addressListList = addressRepository.findByPostcodeAndCommunityAndStreetForGas(postcode, community, street);
                break;
            case Constants.W:
                addressListList = addressRepository.findByPostcodeAndCommunityAndStreetForWater(postcode, community, street);
                break;
            case Constants.F:
                addressListList = addressRepository.findByPostcodeAndCommunityAndStreetForDistrictheating(postcode, community, street);
                break;
            case Constants.TK:
                addressListList = addressRepository.findByPostcodeAndCommunityAndStreetForTelecomm(postcode, community, street);
                break;
            default:
                addressListList = addressRepository.findByPostcodeAndCommunityAndStreet(postcode, community, street);
        }
        return addressListList
                .stream()
                .map(AddressService::toHousenumberUuid)
                .collect(collectingAndThen(toCollection(() -> new TreeSet<>(Comparator.comparing(HousenumberUuidDto::getHousenumber))),
                        ArrayList::new));


    }


}
