/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.config.rabbitMq;

import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;

@Profile("!test & !testCors")
@Configuration
@Log4j2
@EnableRabbit
@Data
public class RabbitMqConfig {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private RabbitMqProperties rabbitMqProperties;

    @PostConstruct
    public void buildAllQueues(){
        log.info("RabbitMqConfig: Configuring all Exchanges and Queues");

        RabbitAdmin admin = new RabbitAdmin(rabbitTemplate);

        // Neuerstellung Exchange und Queues (Import) falls diese nicht vorhanden sind (passiert nur dann!)
        DirectExchange importExchange = new DirectExchange(rabbitMqProperties.getImportExchange());
        log.info("************** Configure Import RabbitMQ **************");
        log.info("ImportExchange: "+ rabbitMqProperties.getImportExchange());
        admin.declareExchange(importExchange);
        Queue importQueue = new Queue(rabbitMqProperties.getImportQueue());
        log.info("ImportQueue: "+ rabbitMqProperties.getImportQueue());
        log.info("ImportKey:   "+ rabbitMqProperties.getImportkey());
        admin.declareQueue(importQueue);
        admin.declareBinding(BindingBuilder.bind(importQueue).to(importExchange).with(rabbitMqProperties.getImportkey()));

        // Neuerstellung Exchange und Queues (Export) falls diese nicht vorhanden sind (passiert nur dann!)
        DirectExchange exportExchange = new DirectExchange(rabbitMqProperties.getExportExchange());
        log.info("************** Configure Export RabbitMQ **************");
        log.info("ExportExchange: "+ rabbitMqProperties.getExportExchange());
        admin.declareExchange(exportExchange);

        for(RabbitMqChannel rabbitMqchannel: rabbitMqProperties.getChannels()) {
            log.info("ExportQueue: "+ rabbitMqchannel.getExportQueue());
            log.info("ExportKey:   "+ rabbitMqchannel.getExportKey());
            Queue exportQueue = new Queue(rabbitMqchannel.getExportQueue());
            admin.declareQueue(exportQueue);
            admin.declareBinding(BindingBuilder.bind(exportQueue).to(exportExchange).with(rabbitMqchannel.getExportKey()));
        }
    }

    public void checkExchangeAndQueueOnRabbitMq(String exportQueue, String exportKey){

        // Neuerstellung Exchange und Queue falls diese nicht vorhanden sind (passiert nur dann!)
        RabbitAdmin admin = new RabbitAdmin(rabbitTemplate);
        DirectExchange exchange =new DirectExchange(rabbitMqProperties.getExportExchange());
        admin.declareExchange(exchange);
        Queue queue = new Queue(exportQueue);
        admin.declareQueue(queue);
        admin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(exportKey));
    }
}