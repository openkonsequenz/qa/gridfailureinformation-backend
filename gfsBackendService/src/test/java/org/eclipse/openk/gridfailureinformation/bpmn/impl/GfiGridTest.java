/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.impl;

import org.eclipse.openk.gridfailureinformation.bpmn.base.*;
import org.eclipse.openk.gridfailureinformation.bpmn.impl.tasks.ProcessHelper;
import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.RefStatus;
import org.eclipse.openk.gridfailureinformation.model.TblFailureInformation;
import org.eclipse.openk.gridfailureinformation.repository.FailureInformationRepository;
import org.eclipse.openk.gridfailureinformation.repository.StatusRepository;
import org.eclipse.openk.gridfailureinformation.service.FailureInformationService;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.FailureInformationDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.Optional;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})
public class GfiGridTest {
    @Qualifier("myFailureInformationService")
    @Autowired
    private FailureInformationService failureInformationService;

    @MockBean
    private ProcessHelper processHelper;
    @MockBean
    private StatusRepository statusRepository;
    @MockBean
    private FailureInformationRepository failureInformationRepository;

    @Test
    public void testProcessGrid() throws ProcessException {
        ProcessGridImpl theGrid = new ProcessGridImpl();
        TestProcessSubject subject = new TestProcessSubject();
        theGrid.recover(subject).start(() -> ProcessStateImpl.UITASK);
        ServiceTaskImpl serviceTask = (ServiceTaskImpl)theGrid.getService();
        assertTrue(serviceTask.leaveStepCalled);
    }

    @Test
    public void testProcessGrid_unreachable() throws ProcessException {
        ProcessGridImpl theGrid = new ProcessGridImpl();
        TestProcessSubject subject = new TestProcessSubject();
        assertThrows(ProcessException.class, () -> theGrid.recover(subject).start(()-> ProcessStateImpl.UNREACHABLE) );
    }

    // wegen intialer Testabdeckung
    @Test
    public void testGfiGridConstr() throws ProcessException {
        ProcessGrid grid = new GfiGrid();
        assertNotNull(grid);
    }

    @Test
    public void shouldEndInQualifyMessageWhenStoredWithCreatedFromDBStateNew() {
        FailureInformationDto dtoToSave = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation failureFromDB = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatusFromDB = MockDataHelper.mockRefStatusCreated();
        refStatusFromDB.setId(GfiProcessState.NEW.getStatusValue());

        when( failureInformationRepository.findByUuid(eq(dtoToSave.getUuid()))).thenReturn(Optional.of(failureFromDB));
        when( statusRepository.findByUuid(eq(failureFromDB.getRefStatusIntern().getUuid()))).thenReturn(Optional.of(refStatusFromDB));

        failureInformationService.updateFailureInfo(dtoToSave);


        verify(processHelper, times(1))
                .storeFailureFromViewModel(any(FailureInformationDto.class));

    }

    @Test
    public void shouldEndInQualifyMessageWhenStoredWithCreatedFromDBStatePlanned() {
        FailureInformationDto dtoToSave = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation failureFromDB = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatusFromDB = MockDataHelper.mockRefStatusCreated();
        refStatusFromDB.setId(GfiProcessState.PLANNED.getStatusValue());

        when( failureInformationRepository.findByUuid(eq(dtoToSave.getUuid()))).thenReturn(Optional.of(failureFromDB));
        when( statusRepository.findByUuid(eq(failureFromDB.getRefStatusIntern().getUuid()))).thenReturn(Optional.of(refStatusFromDB));

        failureInformationService.updateFailureInfo(dtoToSave);

        verify(processHelper, times(1))
                .storeFailureFromViewModel(any(FailureInformationDto.class));

    }

    @Test
    public void shouldCallStoreOnlyOnceWhenSavingWithNewCreated() {
        FailureInformationDto dtoToSave = MockDataHelper.mockFailureInformationDto();
        TblFailureInformation failureFromDB = MockDataHelper.mockTblFailureInformation();
        RefStatus refStatusFromDB = MockDataHelper.mockRefStatusCreated();
        refStatusFromDB.setId(GfiProcessState.NEW.getStatusValue());

        when( failureInformationRepository.findByUuid(eq(dtoToSave.getUuid()))).thenReturn(Optional.of(failureFromDB));
        when( statusRepository.findByUuid(eq(failureFromDB.getRefStatusIntern().getUuid()))).thenReturn(Optional.of(refStatusFromDB));

        failureInformationService.updateFailureInfo(dtoToSave);

        verify(processHelper, times(1)).storeFailureFromViewModel(any(FailureInformationDto.class));
    }
}
