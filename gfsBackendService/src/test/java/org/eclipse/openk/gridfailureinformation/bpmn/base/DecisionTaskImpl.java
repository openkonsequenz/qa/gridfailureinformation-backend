/*
 *******************************************************************************
 * Copyright (c) 2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/

package org.eclipse.openk.gridfailureinformation.bpmn.base;

import org.eclipse.openk.gridfailureinformation.bpmn.base.tasks.DecisionTask;

public class DecisionTaskImpl extends DecisionTask<TestProcessSubject> {

    public DecisionTaskImpl(String description) {
        super(description);
    }

    @Override
    protected void onEnterStep(TestProcessSubject model) throws ProcessException {
        // NOSONAR needs to exist
    }

    @Override
    public OutputPort decide(TestProcessSubject model) throws ProcessException {
        if( model.yes ) {
            return OutputPort.YES;
        }
        if( model.no ) {
            return OutputPort.NO;
        }

        switch( model.decision ) {
            case 1:
                return OutputPort.PORT1;
            case 2:
                return OutputPort.PORT2;
            case 3:
                return OutputPort.PORT3;
            case 4:
                return OutputPort.PORT4;
            case 5:
                return OutputPort.PORT5;
            default:
                throw new ProcessException("Unknown Port");
        }
    }
}
