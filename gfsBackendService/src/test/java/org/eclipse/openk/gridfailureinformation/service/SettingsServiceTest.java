/*
 *******************************************************************************
 * Copyright (c) 2019 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
 */
package org.eclipse.openk.gridfailureinformation.service;

import org.eclipse.openk.gridfailureinformation.config.TestConfiguration;
import org.eclipse.openk.gridfailureinformation.model.TblAddress;
import org.eclipse.openk.gridfailureinformation.repository.AddressRepository;
import org.eclipse.openk.gridfailureinformation.support.MockDataHelper;
import org.eclipse.openk.gridfailureinformation.viewmodel.AddressDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FEInitialContentDto;
import org.eclipse.openk.gridfailureinformation.viewmodel.FESettingsDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@DataJpaTest
@ContextConfiguration(classes = {TestConfiguration.class})

public class SettingsServiceTest {

    @Autowired
    private SettingsService settingsService;

    @Test
    public void shouldGetFESettings() {

        FESettingsDto feSettingsDto = settingsService.getFESettings();

        assertFalse(feSettingsDto.getDetailMapInitialZoom() == null);
        assertFalse(feSettingsDto.getOverviewMapInitialZoom()== null);
        assertFalse(feSettingsDto.getExportChannels() == null);
        assertFalse(feSettingsDto.getOverviewMapInitialLatitude() == null);
        assertFalse(feSettingsDto.getOverviewMapInitialLongitude() == null);
        assertFalse(feSettingsDto.getDataExternInitialVisibility() == null);

    }

    @Test
    public void shouldGetInitialEmailSubjectAndContent() {

        FEInitialContentDto feSettingsDto = settingsService.getInitialEmailSubjectAndContent();

        assertFalse(feSettingsDto.getEmailContentCompleteInit() == null);
        assertFalse(feSettingsDto.getEmailSubjectCompleteInit() == null);
        assertFalse(feSettingsDto.getEmailContentPublishInit() == null);
        assertFalse(feSettingsDto.getEmailSubjectPublishInit() == null);
        assertFalse(feSettingsDto.getEmailSubjectUpdateInit() == null);
        assertFalse(feSettingsDto.getEmailContentUpdateInit() == null);
    }
}
